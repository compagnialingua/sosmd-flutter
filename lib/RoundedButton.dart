import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class RoundedButton extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;

  RoundedButton(this.text, this.onPressed);

  @override
  Widget build(BuildContext context) {
    return RawMaterialButton(
      onPressed: this.onPressed,
      fillColor: Colors.white,
      splashColor: Colors.deepOrange, // Theme.of(context).accentColor,
      child: Center(
        child: Text(
          this.text,
          style: TextStyle(
            color: Colors.black,
            fontSize: 18.0,
            fontFamily: 'helvetica_neue_light',
          ),
          textAlign: TextAlign.center,
        ),
      ),
      shape: new RoundedRectangleBorder(
        borderRadius: new BorderRadius.all(new Radius.circular(8.0)),
      ),
      constraints: BoxConstraints(minHeight: 50.0),
    );
  }
}
