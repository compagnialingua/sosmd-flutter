import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:hello/Strings.dart';

class LicenseScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    String data = """
      <h1>SOS</h1>
      <h2>&copy;2018 Vasco Electronics</h2>
      <dl>By using the Application, you are agreeing to be bound by the terms and conditions of this Agreement.</dl>
      <dl>
        <dt>License</dt>
      </dl>
      <dl>
        <dd>Vasco Electronics LLC grants you a revocable, non-exclusive, non-transferable, limited
          license to download, install and use the Application solely for your personal,
          non-commercial purposes strictly in accordance with the terms of this Agreement.
        </dd>
      </dl>
      <dl>
        <dt>Restrictions</dt>
      </dl>
      <dl>
        <dd>You agree not to, and you will not permit others to:</dd>
        <dd>a) license, sell, rent, lease, assign, distribute, transmit, host, outsource, disclose or
          otherwise commercially exploit the Application or make the Application available to any
          third party.
        </dd>
        <dd>The Restrictions section is for applying certain restrictions on the app usage, e.g. user
          can't sell app, user can't distribute the app. For the full disclosure section, create your
          own EULA.
        </dd>
      </dl>
      <dl>
        <dt>Modifications to Application</dt>
      </dl>
      <dl>
        <dd>Vasco Electronics LLC reserves the right to modify, suspend or discontinue, temporarily or
          permanently, the Application or any service to which it connects, with or without notice and
          without liability to you.
        </dd>
        <dd>The Modifications to Application section is for apps that will be updated or regularly
          maintained.
        </dd>
      </dl>
      <dl>Term and Termination</dl>
      <dl>
        <dd>This Agreement shall remain in effect until terminated by you or Vasco Electronics LLC.</dd>
        <dd>Vasco Electronics LLC may, in its sole discretion, at any time and for any or no reason,
          suspend or terminate this Agreement with or without prior notice.
        </dd>
        <dd>This Agreement will terminate immediately, without prior notice from Vasco Electronics LLC,
          in the event that you fail to comply with any provision of this Agreement. You may also
          terminate this Agreement by deleting the Application and all copies thereof from your mobile
          device or from your desktop.
        </dd>
        <dd>Upon termination of this Agreement, you shall cease all use of the Application and delete
          all copies of the Application from your mobile device or from your desktop.
        </dd>
      </dl>
      <dt>Severability</dt>
      <dl>
        <dd>If any provision of this Agreement is held to be unenforceable or invalid, such provision
          will be changed and interpreted to accomplish the objectives of such provision to the
          greatest extent possible under applicable law and the remaining provisions will continue in
          full force and effect.
        </dd>
      </dl>

      <dl>This app uses the following libraries and third party APIs:</dl>
      <dl>
        <dt>The Android Open Source Project</dt>
      </dl>
      <dl>
        <dd>Copyright (C) 2011 The Android Open Source Project
          Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
          except in compliance with the License. You may obtain a copy of the License at
          http://www.apache.org/licenses/LICENSE-2.0 Unless required by applicable law or agreed to in
          writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
          WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the
          specific language governing permissions and
          limitations under the License.
        </dd>
      </dl>
    """;
    return Scaffold(
      appBar: AppBar(
        title: Text(AppStrings.of(context).trans('license_screen_title')),
      ),
      body: SingleChildScrollView(
          child: Stack(children: <Widget>[
        Column(children: [
          Padding(
            padding: EdgeInsets.all(10),
            child: SizedBox(
              child: Center(
                child: Html(data: data),
              ),
            ),
          )
        ]),
      ])),
    );
  }
}
