import 'package:flutter/material.dart';
import 'package:hello/ConfirmDialog.dart';
import 'package:hello/LicenseScreen.dart';
import 'package:hello/RoundedButton.dart';
import 'package:hello/SettingsDialog.dart';
import 'package:hello/Strings.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:permission/permission.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  onClickSendSos() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    bool addLocation = prefs.getBool('addLocation') ?? false;

    List<PermissionName> permissionNameList = addLocation
        ? [PermissionName.SMS, PermissionName.Contacts, PermissionName.Location]
        : [PermissionName.SMS, PermissionName.Contacts];

    final result = await Permission.requestPermissions(permissionNameList);

    if (result
            .where((permission) =>
                permission.permissionStatus == PermissionStatus.allow)
            .length ==
        result.length) {
      showDialog(context: context, builder: (_) => ConfirmDialog());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: Container(
        padding: MediaQuery.of(context).padding,
        decoration: BoxDecoration(
          gradient: LinearGradient(
              colors: [
                Color.fromRGBO(0xF7, 0x57, 0x57, 1.0),
                Color.fromRGBO(0x92, 0x17, 0x05, 1.0)
              ],
              begin: const FractionalOffset(0, 0),
              end: const FractionalOffset(0, 1),
              stops: [0, 1],
              tileMode: TileMode.clamp),
        ),
        child: Stack(
          alignment: const Alignment(1, -1),
          children: [
            Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Image.asset('assets/images/logo.png',
                      width: 192, height: 192),
                  Container(
                    margin: EdgeInsets.all(16),
                    child: RawMaterialButton(
                      onPressed: () => showDialog(
                          context: context, builder: (_) => SettingsDialog()),
                      child: Container(
                        margin: EdgeInsets.all(8),
                        child: Column(
                          children: <Widget>[
                            Container(
                              margin: const EdgeInsets.only(bottom: 16),
                              child: Image.asset('assets/images/edit.png',
                                  width: 60, height: 60),
                            ),
                            Text(
                                AppStrings.of(context)
                                    .trans('edit_phone_number_and_text'),
                                style: Theme.of(context)
                                    .primaryTextTheme
                                    .caption
                                    .copyWith(
                                        color: Colors.white, fontSize: 20)),
                          ],
                        ),
                      ),
                      constraints: BoxConstraints(minWidth: 50, minHeight: 50),
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(top: 8, left: 30, right: 30),
                    child: RoundedButton(
                        AppStrings.of(context).trans('send_sos'),
                        () => onClickSendSos()),
                    constraints: BoxConstraints(
                        minWidth: double.infinity, minHeight: 70),
                  ),
                ],
              ),
            ),
            Container(
              child: RawMaterialButton(
                onPressed: () => Navigator.push(context,
                    MaterialPageRoute(builder: (context) => LicenseScreen())),
                padding: EdgeInsets.all(10),
                child: IconTheme(
                  data: IconThemeData(color: Colors.white),
                  child: Icon(Icons.info),
                ),
                constraints: BoxConstraints(minWidth: 50, minHeight: 50),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
