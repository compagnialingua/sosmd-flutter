import 'package:flutter/material.dart';
import 'package:hello/CustomShowDialog.dart';
import 'package:hello/RoundedButton.dart';
import 'package:hello/Strings.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsDialog extends StatefulWidget {
  SettingsDialog();

  @override
  _SettingsDialogState createState() => _SettingsDialogState();
}

class _SettingsDialogState extends State<SettingsDialog> {
  var phoneTextFieldController = new TextEditingController();
  var messageTextFieldController = new TextEditingController();
  var addLocationValue = false;

  saveToLocalData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('phoneNumber', phoneTextFieldController.text);
    await prefs.setString('message', messageTextFieldController.text);
    await prefs.setBool('addLocation', addLocationValue);
  }

  loadFromLocalData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    phoneTextFieldController.text = prefs.getString('phoneNumber') ?? '';
    messageTextFieldController.text = prefs.getString('message') ?? '';
    addLocationValue = prefs.getBool('addLocation') ?? false;
  }

  onSaveButtonPressed() {
    Navigator.pop(context);
    saveToLocalData();
  }

  @override
  void initState() {
    super.initState();
    loadFromLocalData();
  }

  @override
  Widget build(BuildContext context) {
    return CustomAlertDialog(
      content: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Stack(children: <Widget>[
              Container(
                margin: EdgeInsets.only(top: 30),
                padding: EdgeInsets.fromLTRB(16, 30, 16, 0),
                child: Column(
                  children: <Widget>[
                    Text(AppStrings.of(context)
                        .trans('provide_number_with_country_code')),
                    TextField(
                      autofocus: true,
                      controller: phoneTextFieldController,
                      decoration: InputDecoration(
                        hintText:
                            AppStrings.of(context).trans('enter_phone_number'),
                      ),
                    ),
                    TextField(
                      controller: messageTextFieldController,
                      decoration: InputDecoration(
                          hintText: AppStrings.of(context).trans('enter_text')),
                    ),
                    Row(mainAxisSize: MainAxisSize.max, children: <Widget>[
                      Checkbox(
                          value: addLocationValue,
                          onChanged: (bool newValue) {
                            setState(() {
                              addLocationValue = newValue;
                            });
                          }),
                      Text(AppStrings.of(context).trans('add_location')),
                    ])
                  ],
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  color: Colors.white,
                ),
              ),
              Container(
                padding: EdgeInsets.all(12),
                decoration: new BoxDecoration(
                  color: Colors.red,
                  shape: BoxShape.circle,
                ),
                child: new Center(
                  child: Image.asset('assets/images/edit.png',
                      width: 36, height: 36),
                ),
              ),
            ]),
            Container(
              margin: EdgeInsets.only(top: 8),
              child: RoundedButton(AppStrings.of(context).trans('save'),
                  () => onSaveButtonPressed()),
            ),
          ],
        ),
      ),
    );
  }
}
