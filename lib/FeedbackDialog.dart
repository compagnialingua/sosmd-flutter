import 'package:flutter/material.dart';
import 'package:hello/CustomShowDialog.dart';
import 'package:hello/RoundedButton.dart';

class DialogWithMessage extends StatelessWidget {
  DialogWithMessage(this.message, this.buttonText);

  final String message;
  final String buttonText;

  @override
  Widget build(BuildContext context) {
    return CustomAlertDialog(
      content: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Stack(children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(16, 30, 16, 30),
                child: Center(
                  child: Text(this.message),
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  color: Colors.white,
                ),
              ),
            ]),
            Container(
              margin: EdgeInsets.only(top: 8),
              child: RoundedButton(this.buttonText, () => Navigator.pop(context)),
            ),
          ],
        ),
      ),
    );
  }
}
