import 'package:flutter/material.dart';
import 'package:hello/CustomShowDialog.dart';
import 'package:hello/FeedbackDialog.dart';
import 'package:hello/RoundedButton.dart';
import 'package:geolocator/geolocator.dart';
import 'package:hello/Strings.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sms/sms.dart';

class ConfirmDialog extends StatefulWidget {
  ConfirmDialog();

  @override
  _ConfirmDialogState createState() => _ConfirmDialogState();
}

// () => Timer.periodic(new Duration(seconds: 10), (timer) => sendMessage()))

class _ConfirmDialogState extends State<ConfirmDialog>
    with TickerProviderStateMixin {
  AnimationController controller;

  static const Duration timerDuration = Duration(seconds: 10); // in seconds

  String get timerString {
    Duration duration = controller.duration * controller.value;
    return '${(timerDuration.inSeconds - duration.inSeconds).toString()}';
  }

  sendSms(String phoneNumber, String message) {
    SmsMessage smsMessage = new SmsMessage(phoneNumber, message);
    smsMessage.onStateChanged.listen((state) {
      if (state == SmsMessageState.Sending) {
      } else if (state == SmsMessageState.Sent) {
        Navigator.pop(context);
        showDialog(
            context: context,
            builder: (_) => DialogWithMessage(
                AppStrings.of(context).trans('sms_sended'), 'Close'));
      } else if (state == SmsMessageState.Delivered) {
      } else if (state == SmsMessageState.Fail) {
        Navigator.pop(context);
        showDialog(
            context: context,
            builder: (_) => DialogWithMessage(
                AppStrings.of(context).trans('sms_sending_fail'), 'Close'));
      }
    });
    SmsSender sender = new SmsSender();
    sender.sendSms(smsMessage);
  }

  sendMessage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String phoneNumber = prefs.getString('phoneNumber') ?? '';
    String message = prefs.getString('message') ?? '';
    bool addLocation = prefs.getBool('addLocation') ?? false;

    Position position;

    if (addLocation) {
      Geolocator geolocator = Geolocator()..forceAndroidLocationManager = true;
      GeolocationStatus geolocationStatus =
          await geolocator.checkGeolocationPermissionStatus();
      if (geolocationStatus == GeolocationStatus.granted) {
        position = await Geolocator()
            .getLastKnownPosition(desiredAccuracy: LocationAccuracy.high);
        if (position != null) {
          message = message +
              '\nhttp://maps.google.com/?q=${position.latitude},${position.longitude}';
        }
      }
    }

    sendSms(phoneNumber, message);
  }

  @override
  void initState() {
    super.initState();
    controller = AnimationController(vsync: this, duration: timerDuration);
    controller.addListener(() => this.setState(() {}));
    controller.addListener(() {
      if (this.controller.isCompleted) {
        sendMessage();
      }
    });
    controller.forward(from: 0.0);
  }

  @override
  Widget build(BuildContext context) {
    return CustomAlertDialog(
      content: Container(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Stack(children: <Widget>[
              Container(
                padding: EdgeInsets.fromLTRB(16, 30, 16, 30),
                child: Center(
                  child: AnimatedBuilder(
                      animation: controller,
                      builder: (BuildContext context, Widget child) {
                        return new Text(
                            AppStrings.of(context).trans('sms_pending') +
                                ' $timerString');
                      }),
                ),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(8.0)),
                  color: Colors.white,
                ),
              ),
            ]),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  flex: 1,
                  child: Container(
                    padding: EdgeInsets.only(right: 2, top: 4),
                    child: RoundedButton(AppStrings.of(context).trans('cancel'),
                        () => Navigator.pop(context)),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Container(
                    padding: EdgeInsets.only(left: 2, top: 4),
                    child: RoundedButton(
                        AppStrings.of(context).trans('send_now'),
                        () => sendMessage()),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
  }
}
